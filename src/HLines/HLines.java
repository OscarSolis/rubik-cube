package HLines;
// HLines.java: Perspective drawing with hidden-line elimination.

// Copied from Section 6.8 of
//    Ammeraal, L. and K. Zhang (2007). Computer Graphics for Java Programmers, 2nd Edition,
//       Chichester: John Wiley.

// When you compile this program, the .class or the .java files of the
// following classes should also be in your current directory:
//       CvHLines, Fr3D, Fr3DH, Polygon3D, Obj3D, Input, Canvas3D, 
//       Point3D, Point2D, Triangle, Tria, Tools2D, HPGL.
import java.awt.*;
public class HLines extends Frame
{  public static void main(String[] args)
   {  new Fr3DH(args.length > 0 ? args[0] : null, new CvHLines(),
         "Hidden-lines algorithm");
   }
}
