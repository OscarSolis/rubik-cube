/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HLines;
// CvPainter.java: Used in the file Painter.java.
import java.awt.*;
import java.util.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;

class CvPainter extends Canvas3D implements MouseListener, MouseMotionListener {

    private int maxX, maxY, centerX, centerY;
    private Obj3D obj;
    private Point2D imgCenter;
    int x1, y1, x2, y2;

    private BufferedImage imagenAux;
    private Graphics gAux;
    private Dimension dimAux;
    
   
    Obj3D getObj() {
        return obj;
    }

    void setObj(Obj3D obj) {
        this.obj = obj;
    }

    int iX(float x) {
        return Math.round(centerX + x - imgCenter.x);
    }

    int iY(float y) {
        return Math.round(centerY - y + imgCenter.y);
    }

    public CvPainter() {
        this.addMouseListener(this);
        this.addMouseMotionListener(this);
    }

    void sort(Tria[] tr, int[] colorCode, float[] zTr, int l, int r) {
        int i = l, j = r, wInt;
        float x = zTr[(i + j) / 2], w;
        Tria wTria;
        do {
            while (zTr[i] < x) {
                i++;
            }
            while (zTr[j] > x) {
                j--;
            }
            if (i < j) {
                w = zTr[i];
                zTr[i] = zTr[j];
                zTr[j] = w;
                wTria = tr[i];
                tr[i] = tr[j];
                tr[j] = wTria;
                wInt = colorCode[i];
                colorCode[i] = colorCode[j];
                colorCode[j] = wInt;
                i++;
                j--;
            } else if (i == j) {
                i++;
                j--;
            }
        } while (i <= j);
        if (l < j) {
            sort(tr, colorCode, zTr, l, j);
        }
        if (i < r) {
            sort(tr, colorCode, zTr, i, r);
        }
    }

    void vp(float dTheta, float dPhi, float fRho) // Viewpoint
    {
        Obj3D obj = getObj();

        if (obj == null || !obj.vp(this, dTheta, dPhi, fRho)) {
            Toolkit.getDefaultToolkit().beep();
        }
    }

    @Override
    public void paint(Graphics g) {

        if (obj == null) {
            return;
        }
       
        Dimension dim = getSize();
        if (gAux == null || imagenAux == null || dim.width != dimAux.width || dim.height != dimAux.height) {
            dimAux = dim;
            imagenAux = (BufferedImage) createImage(dimAux.width, dimAux.height);
            gAux = imagenAux.getGraphics();
        }

        Vector polyList = obj.getPolyList();
        if (polyList == null) {
            return;
        }
        int nFaces = polyList.size();
        if (nFaces == 0) {
            return;
        }

        maxX = dim.width - 1;
        maxY = dim.height - 1;
        centerX = maxX / 2;
        centerY = maxY / 2;
        // ze-axis towards eye, so ze-coordinates of
        // object points are all negative. 
        // obj is a java object that contains all data:
        // - Vector w        (world coordinates)
        // - Array e         (eye coordinates)
        // - Array vScr      (screen coordinates)
        // - Vector polyList (Polygon3D objects)

        // Every Polygon3D value contains:
        // - Array 'nrs' for vertex numbers
        // - Values a, b, c, h for the plane ax+by+cz=h.
        // - Array t (with nrs.length-2 elements of type Tria)
        // Every Tria value consists of the three vertex
        // numbers iA, iB and iC.
        obj.eyeAndScreen(dim);
        // Computation of eye and screen coordinates.

        imgCenter = obj.getImgCenter();
        obj.planeCoeff();    // Compute a, b, c and h.

        // Construct an array of triangles in
        // each polygon and count the total number 
        // of triangles:
        int nTria = 0;
        for (int j = 0; j < nFaces; j++) {
            Polygon3D pol = (Polygon3D) (polyList.elementAt(j));
            if (pol.getNrs().length < 3 || pol.getH() >= 0) {
                continue;
            }
            pol.triangulate(obj);
            nTria += pol.getT().length;
        }

        Tria[] tr = new Tria[nTria];
        int[] colorCode = new int[nTria];
        float[] zTr = new float[nTria];
        int iTria = 0;
        Point3D[] e = obj.getE();
        Point2D[] vScr = obj.getVScr();

        for (int j = 0; j < nFaces; j++) {
            int cCode = 0;
            Polygon3D pol = (Polygon3D) (polyList.elementAt(j));
            if (pol.getNrs().length < 3 || pol.getH() >= 0) {
                continue;
            }
            //color cara abajo
            if (j < 18) {
                cCode = Color.RED.getRGB();
            } // color cara atras
            else if(j>=18 && j<=35){
                cCode = Color.BLUE.getRGB();
            }//color cara derecha
            else if (j >=36 && j <= 53) {
                cCode = Color.GREEN.getRGB();
            } //color cara izquierda
            else if (j >= 54 && j <= 71) {
                cCode = Color.WHITE.getRGB();
            }//color cara enfrente
            else if (j >= 72 && j <= 89) {
                cCode = Color.YELLOW.getRGB();
            }//color arriba
            else{
                cCode = Color.GRAY.getRGB();
            }
            Tria[] t = pol.getT();
            for (int i = 0; i < t.length; i++) {
                Tria tri = t[i];
                tr[iTria] = tri;
                colorCode[iTria] = cCode;
                float zA = e[tri.iA].z, zB = e[tri.iB].z,
                        zC = e[tri.iC].z;
                zTr[iTria++] = zA + zB + zC;
            }
        }

        sort(tr, colorCode, zTr, 0, nTria - 1);

        for (iTria = 0; iTria < nTria; iTria++) {
            Tria tri = tr[iTria];
            Point2D a = vScr[tri.iA],
                    b = vScr[tri.iB],
                    c = vScr[tri.iC];
            int cCode = colorCode[iTria];
            g.setColor(new Color(cCode));
            //gAux.setColor(new Color(cCode));
            int[] x = {iX(a.x), iX(b.x), iX(c.x)};
            int[] y = {iY(a.y), iY(b.y), iY(c.y)};
            gAux.fillPolygon(x, y, 3);
            g.fillPolygon(x, y, 3);
            //g.drawImage(imagenAux, 0, 0, null);

        }

    }

    public void mouseMoved(MouseEvent e) {
    }

    public void mouseClicked(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }
    
    public void mousePressed(MouseEvent evt) {
        x1 = evt.getX();
        y1 = evt.getY();
    }

    public void mouseDragged(MouseEvent ev) {
        x2 = ev.getX();
        y2 = ev.getY();
        float y, x;
        y = (float) ((y2 - y1) * 0.0001);
        vp(0, y, 1);
        x = (float) ((x1 - x2) * 0.0001);
        vp(x, 0, 1);
    }
}
