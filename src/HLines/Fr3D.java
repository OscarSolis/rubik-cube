package HLines;
// Fr3D.java: Frame class to deal with menu commands and other
//    user actions.

// Copied from Section 5.5 of
//    Ammeraal, L. and K. Zhang (2007). Computer Graphics for Java Programmers, 2nd Edition,
//       Chichester: John Wiley.
import java.awt.*;
import java.awt.event.*;
import java.util.*;

class Fr3D extends Frame implements ActionListener {

    String mA[][] = {
        {"1,7", "41,46", "11,17"},
        {"71,76", "81,86", "51,56"},
        {"31,37", "61,66", "21,27"}
    };

    String mE[][] = {
        {"101,107", "141,146", "111,117"},
        {"171,176", "181,186", "151,156"},
        {"131,137", "161,166", "121,127"}
    };

    String mS[][] = {
        {"201,207", "241,246", "211,217"},
        {"271,276", "281,286", "251,256"},
        {"231,237", "261,266", "221,227"}
    };

    int cont = 0;
    protected MenuItem open, exit, eyeUp, eyeDown, eyeLeft, eyeRight,
            incrDist, decrDist;
    
    Button moverAD, moverED, moverSD, moverAI, moverEI, moverSI, arribaD, abajoD, arribaE, abajoE, arribaI, abajoI, btn1, btn2, btn3, reiniciar;
    protected String sDir;
    Canvas3D cv;
    protected Menu mF, mV;
    String filename;
    Vector puntos, v;
    Hilo h = new Hilo(0);

    Fr3D(String argFileName, Canvas3D cv, String textTitle) {
        super(textTitle);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
        
        this.cv = cv;
        MenuBar mBar = new MenuBar();
        setMenuBar(mBar);

        mF = new Menu("File");
        mV = new Menu("View");
        mBar.add(mF);
        mBar.add(mV);

        open = new MenuItem("Open",
                new MenuShortcut(KeyEvent.VK_O));
        eyeDown = new MenuItem("Viewpoint Down",
                new MenuShortcut(KeyEvent.VK_DOWN));
        eyeUp = new MenuItem("Viewpoint Up",
                new MenuShortcut(KeyEvent.VK_UP));
        eyeLeft = new MenuItem("Viewpoint to Left",
                new MenuShortcut(KeyEvent.VK_LEFT));
        eyeRight = new MenuItem("Viewpoint to Right",
                new MenuShortcut(KeyEvent.VK_RIGHT));

        incrDist = new MenuItem("Increase viewing distance",
                new MenuShortcut(KeyEvent.VK_INSERT));
        decrDist = new MenuItem("Decrease viewing distance",
                new MenuShortcut(KeyEvent.VK_DELETE));
        exit = new MenuItem("Exit",
                new MenuShortcut(KeyEvent.VK_Q));

        initComponent();
        mF.add(open);
        mF.add(exit);
        mV.add(eyeDown);
        mV.add(eyeUp);
        mV.add(eyeLeft);
        mV.add(eyeRight);
        mV.add(incrDist);
        mV.add(decrDist);
        open.addActionListener(this);
        exit.addActionListener(this);
        eyeDown.addActionListener(this);
        eyeUp.addActionListener(this);
        eyeLeft.addActionListener(this);
        eyeRight.addActionListener(this);
        incrDist.addActionListener(this);
        decrDist.addActionListener(this);
        add("Center", cv);
        Dimension dim = getToolkit().getScreenSize();
        setSize(dim.width / 2, dim.height / 2);
        setLocation(dim.width / 4, dim.height / 4);

        if (argFileName != null) {
            Obj3D obj = new Obj3D();
            if (obj.read(argFileName)) {
                cv.setObj(obj);
                cv.repaint();
            }
        }
        cargarArchivo();
        //cv.setBackground(new Color(180, 180, 255));
        cv.setBackground(Color.BLACK);
        show();
    }
    /**
     * metodo que incializa los compentes del Frame::botones, paneles, eventos
     */
    void initComponent() {
        Panel padre = new Panel();
        Panel panelComponentes = new Panel();
        Panel paux1 = new Panel();
        paux1.setBackground(Color.BLACK);
        padre.setLayout(new GridLayout(2, 1));
        panelComponentes.setLayout(new GridLayout(5, 3, 2, 2));
        moverAD = new Button("<--");
        arribaD = new Button("arriba");
        moverED = new Button("<--");
        abajoD = new Button("abajo");
        moverSD = new Button("<--");

        arribaE = new Button("arriba");
        btn1 = new Button();
        btn1.setEnabled(false);
        btn2 = new Button();
        btn2.setEnabled(false);
        btn3 = new Button();
        btn3.setEnabled(false);
        abajoE = new Button("abajo");

        moverAI = new Button("-->");
        arribaI = new Button("arriba");
        moverEI = new Button("-->");
        abajoI = new Button("abajo");
        moverSI = new Button("-->");
        
        reiniciar = new Button("reiniciar");
        reiniciar.setForeground(Color.WHITE);
        
        panelComponentes.add(moverSD);
        panelComponentes.add(arribaE);
        panelComponentes.add(moverSI);

        panelComponentes.add(arribaD);
        panelComponentes.add(btn1);
        panelComponentes.add(arribaI);

        panelComponentes.add(moverED);
        panelComponentes.add(btn2);
        panelComponentes.add(moverEI);

        panelComponentes.add(abajoD);
        panelComponentes.add(btn3);
        panelComponentes.add(abajoI);

        panelComponentes.add(moverAD);
        panelComponentes.add(abajoE);
        panelComponentes.add(moverAI);

        paux1.add(reiniciar);
        
        padre.add(panelComponentes);
        padre.add(paux1);
        add(padre, BorderLayout.EAST);

        moverSI.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                h = new Hilo(6);
            }
        });

        moverSD.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                h = new Hilo(5);
            }
        });

        moverED.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                h = new Hilo(3);
            }
        });

        moverEI.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                h = new Hilo(4);
            }
        });

        moverAD.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                h = new Hilo(1);
            }
        });

        moverAI.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                h = new Hilo(2);
            }
        });

        arribaD.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                h = new Hilo(7);
            }
        });

        abajoD.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                h = new Hilo(8);
            }
        });

        arribaE.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                h = new Hilo(9);
            }
        });

        abajoE.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                h = new Hilo(10);
            }
        });

        arribaI.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                h = new Hilo(11);
            }
        });

        abajoI.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                h = new Hilo(12);
            }
        });
        
        reiniciar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mA = resetMA();
                mE = resetME();
                mS = resetMS();
                cargarArchivo();
            }

           
        });
        

    }

    public void cargarArchivo() {
     Obj3D obj = new Obj3D();
        if (obj.read("src/HLines/cubo.dat")) {
            puntos = obj.getV();
            cv.setObj(obj);
            cv.repaint();
        }           
    }
    
    public String[][] resetMA(){
        String mA[][] = {
            {"1,7", "41,46", "11,17"},
            {"71,76", "81,86", "51,56"},
            {"31,37", "61,66", "21,27"}
        };
        return mA;
    }
    
    public String[][] resetME(){
        String mE[][] = {
            {"101,107", "141,146", "111,117"},
            {"171,176", "181,186", "151,156"},
            {"131,137", "161,166", "121,127"}
        };
        return mE;
    }
    
    public String[][] resetMS(){
        String mS[][] = {
            {"201,207", "241,246", "211,217"},
            {"271,276", "281,286", "251,256"},
            {"231,237", "261,266", "221,227"}
        };
        return mS;
    }
    
    void vp(float dTheta, float dPhi, float fRho) // Viewpoint
    {
        Obj3D obj = cv.getObj();
        if (obj == null || !obj.vp(cv, dTheta, dPhi, fRho)) {
            Toolkit.getDefaultToolkit().beep();
        }
    }

    public void actionPerformed(ActionEvent ae) {
        if (ae.getSource() instanceof MenuItem) {
            MenuItem mi = (MenuItem) ae.getSource();
            if (mi == open) {
                FileDialog fDia = new FileDialog(Fr3D.this,
                        "Open", FileDialog.LOAD);
                fDia.setDirectory(sDir);
                fDia.setFile("*.dat");
                fDia.show();
                String sDir1 = fDia.getDirectory();
                String sFile = fDia.getFile();
                String fName = sDir1 + sFile;
                Obj3D obj = new Obj3D();
                if (obj.read(fName)) {
                    sDir = sDir1;
                    puntos = obj.getV();
                    cv.setObj(obj);
                    cv.repaint();
                }
            } else if (mi == exit) {
                System.exit(0);
            } else if (mi == eyeDown) {
                vp(0, .1F, 1);
            } else if (mi == eyeUp) {
                vp(0, -.1F, 1);
            } else if (mi == eyeLeft) {
                vp(-.1F, 0, 1);
            } else if (mi == eyeRight) {
                vp(.1F, 0, 1);
            } else if (mi == incrDist) {
                vp(0, 0, 2);
            } else if (mi == decrDist) {
                vp(0, 0, .5F);
            }
        }

    }

    class Hilo extends Thread {
        int modalidad;
        Hilo(int modalidad) {
            this.modalidad = modalidad;
            this.start();
        }
        @Override
        public void run() {
            //-1 derecha
            //1 izquierda
            switch (modalidad) {
                //abajo derecha e izquierda
                case 1:
                    moverIzDer(-1, mA);
                    mA = girarMatrizDerechaIzquierda(mA);
                    break;
                case 2:
                    moverIzDer(1, mA);
                    for (int i = 0; i < 3; i++)
                        mA = girarMatrizDerechaIzquierda(mA);
                    break;
                //enmedio derecha e izquierda
                case 3:
                    moverIzDer(-1, mE);
                    mE = girarMatrizDerechaIzquierda(mE);
                    break;
                case 4:
                    moverIzDer(1, mE);
                    for (int i = 0; i < 3; i++) 
                        mE = girarMatrizDerechaIzquierda(mE);
                    break;
                //arriba derecha e izquierda
                case 5:
                    moverIzDer(-1, mS);
                    mS = girarMatrizDerechaIzquierda(mS);
                    break;
                case 6:
                    moverIzDer(1, mS);
                    for (int i = 0; i < 3; i++)
                        mS = girarMatrizDerechaIzquierda(mS);
                    break;
                //mover Arriba derecha    
                case 7:
                    moverArribaAbajo(-1, 0);
                    girarMatrizArribaAbajo(0);
                    break;
                //mover Abajo derecha    
                case 8:
                    moverArribaAbajo(1, 0);
                    for (int i = 0; i < 3; i++) 
                        girarMatrizArribaAbajo(0);
                    break;
                //mover Arriba enmedio
                case 9:
                    moverArribaAbajo(-1, 1);
                    girarMatrizArribaAbajo(1);
                    break;
                //mover Abajo enmedio    
                case 10:
                    moverArribaAbajo(1, 1);
                    for (int i = 0; i < 3; i++) 
                        girarMatrizArribaAbajo(1);
                    break;
                //mover Arriba izquierda
                case 11:
                    moverArribaAbajo(-1, 2);
                    girarMatrizArribaAbajo(2);
                   break;
                //mover Abajo izquierda
                case 12:
                    moverArribaAbajo(1, 2);
                    for (int i = 0; i < 3; i++) 
                        girarMatrizArribaAbajo(2);
                    break;
            }
        }
        /**
         * Metodo para mover las caras de arriba a abajo
         * @param x direccion a mover -1 hacia arriba, 1 hacia abajo
         * @param y caras a mover con respecto a las matricez mA, mE, MS
         */
        public void moverArribaAbajo(int x, int y){
                Rota3D.initRotate(new Point3D(0, 0, 0), new Point3D(0, 1, 0), (Math.PI / 20) * x);
            for (int j = 0; j < 10; j++) {
                try {
                    for (int i = 0; i <= puntos.size(); i++) {
                        if (i >= Integer.parseInt(mA[0][y].split(",")[0]) && i <= Integer.parseInt(mA[0][y].split(",")[1])) {
                            puntos(i);
                        } else if (i >= Integer.parseInt(mA[1][y].split(",")[0]) && i <= Integer.parseInt(mA[1][y].split(",")[1])) {
                            puntos(i);
                        } else if (i >= Integer.parseInt(mA[2][y].split(",")[0]) && i <= Integer.parseInt(mA[2][y].split(",")[1])) {
                            puntos(i);
                        } else if (i >= Integer.parseInt(mE[0][y].split(",")[0]) && i <= Integer.parseInt(mE[0][y].split(",")[1])) {
                            puntos(i);
                        } else if (i >= Integer.parseInt(mE[1][y].split(",")[0]) && i <= Integer.parseInt(mE[1][y].split(",")[1])) {
                            puntos(i);
                        } else if (i >= Integer.parseInt(mE[2][y].split(",")[0]) && i <= Integer.parseInt(mE[2][y].split(",")[1])) {
                            puntos(i);
                        } else if (i >= Integer.parseInt(mS[0][y].split(",")[0]) && i <= Integer.parseInt(mS[0][y].split(",")[1])) {
                            puntos(i);
                        } else if (i >= Integer.parseInt(mS[1][y].split(",")[0]) && i <= Integer.parseInt(mS[1][y].split(",")[1])) {
                            puntos(i);
                        } else if (i >= Integer.parseInt(mS[2][y].split(",")[0]) && i <= Integer.parseInt(mS[2][y].split(",")[1])) {
                            puntos(i);
                        }
                    }
                    cv.repaint();
                    Thread.sleep(100);
                } catch (Exception e) {
                }
            }
        }
        /**
         * Metodo para mover las caras de derecha a izquierda
         * @param x direccion en las que se moveran las caras -1 derecha, 1 izquierda
         * @param m matriz que indica que cara moveremos la de arriba, enmedio o abajo
         */
        public void moverIzDer(int x, String m[][]) {
            Rota3D.initRotate(new Point3D(0, 0, 0), new Point3D(0, 0, 0), (Math.PI / 20) * x);
            for (int j = 0; j < 10; j++) {
                try {
                    for (int i = 0; i <= puntos.size(); i++) {
                        if (i >= Integer.parseInt(m[0][0].split(",")[0]) && i <= Integer.parseInt(m[0][0].split(",")[1])) {
                            puntos(i);
                        } else if (i >= Integer.parseInt(m[0][1].split(",")[0]) && i <= Integer.parseInt(m[0][1].split(",")[1])) {
                            puntos(i);
                        } else if (i >= Integer.parseInt(m[0][2].split(",")[0]) && i <= Integer.parseInt(m[0][2].split(",")[1])) {
                            puntos(i);
                        } else if (i >= Integer.parseInt(m[1][0].split(",")[0]) && i <= Integer.parseInt(m[1][0].split(",")[1])) {
                            puntos(i);
                        } else if (i >= Integer.parseInt(m[1][1].split(",")[0]) && i <= Integer.parseInt(m[1][1].split(",")[1])) {
                            puntos(i);
                        } else if (i >= Integer.parseInt(m[1][2].split(",")[0]) && i <= Integer.parseInt(m[1][2].split(",")[1])) {
                            puntos(i);
                        } else if (i >= Integer.parseInt(m[2][0].split(",")[0]) && i <= Integer.parseInt(m[2][0].split(",")[1])) {
                            puntos(i);
                        } else if (i >= Integer.parseInt(m[2][1].split(",")[0]) && i <= Integer.parseInt(m[2][1].split(",")[1])) {
                            puntos(i);
                        } else if (i >= Integer.parseInt(m[2][2].split(",")[0]) && i <= Integer.parseInt(m[2][2].split(",")[1])) {
                            puntos(i);
                        }
                    }
                    cv.repaint();
                    Thread.sleep(100);

                } catch (Exception e) {
                }
            }

        }
        /**
         * rota un punto con respecto al arreglo de coordenadas en la posicion i
         * @param i posicion del arreglo a mover
         */
        public void puntos(int i) {
            if (puntos.elementAt(i) != null) {
                Point3D p = Rota3D.rotate((Point3D) puntos.elementAt(i));
                puntos.setElementAt(p, i);
            }
        }

        public String[][] girarMatrizDerechaIzquierda(String a[][]) {
            String[][] b = new String[a.length][a[0].length];
            int rota = 3;
            for (int i = 0; i < rota; ++i) {
                for (int j = 0; j < rota; ++j) {
                    b[i][j] = a[(rota - 1) - j][i];
                }
            }
            return b;
        }
        
        public void girarMatrizArribaAbajo(int x){
            String v[] = new String[9];
            int cont=0;
            for(int i=0; i<3; i++){
                v[cont] = mA[i][x];
                cont ++;
            }
            for(int i=0; i<3; i++){
                v[cont] = mE[i][x];
                cont ++;
            }
            for(int i=0; i<3; i++){
                v[cont] = mS[i][x];
                cont ++;
            }
            mS[0][x] = v[8];
            mA[0][x] = v[6];
            mA[2][x] = v[0];
            mS[2][x] = v[2];
            mS[1][x] = v[5];
            mE[0][x] = v[7];
            mA[1][x] = v[3];
            mE[2][x] = v[1];
        }
    }
}
